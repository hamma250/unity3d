FROM ubuntu:16.04

ARG DOWNLOAD_URL
ARG COMPONENTS=Unity,Android
ARG ANDROID_PLATFORM=27
ARG NDK_VERSION=r13b

RUN apt-get update -qq; \
    apt-get install -qq -y \
    gconf-service \
    lib32gcc1 \
    lib32stdc++6 \
    libasound2 \
    libarchive13 \
    libc6 \
    libc6-i386 \
    libcairo2 \
    libcap2 \
    libcups2 \
    libdbus-1-3 \
    libexpat1 \
    libfontconfig1 \
    libfreetype6 \
    libgcc1 \
    libgconf-2-4 \
    libgdk-pixbuf2.0-0 \
    libgl1-mesa-glx \
    libglib2.0-0 \
    libglu1-mesa \
    libgtk2.0-0 \
    libgtk3.0 \
    libnspr4 \
    libnss3 \
    libpango1.0-0 \
    libsoup2.4-1 \
    libstdc++6 \
    libx11-6 \
    libxcomposite1 \
    libxcursor1 \
    libxdamage1 \
    libxext6 \
    libxfixes3 \
    libxi6 \
    libxrandr2 \
    libxrender1 \
    libxtst6 \
    zlib1g \
    debconf \
    npm \
    xdg-utils \
    lsb-release \
    libpq5 \
    xvfb \
    wget \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN wget -nv ${DOWNLOAD_URL} -O UnitySetup && \
    # compare sha1 if given
    if [ -n "${SHA1}" -a "${SHA1}" != "" ]; then \
     echo "${SHA1}  UnitySetup" | shasum -a 1 --check -; \
    else \
     echo "no sha1 given, skipping checksum"; \
    fi && \
    # make executable
    chmod +x UnitySetup && \
    # agree with license
    echo y | \
    # install unity with required components
    ./UnitySetup --unattended \
    --install-location=/opt/Unity \
    --verbose \
    --download-location=/tmp/unity \
    --components=$COMPONENTS && \
    # remove setup
    rm UnitySetup && \
    # make a directory for the certificate Unity needs to run
    mkdir -p $HOME/.local/share/unity3d/Certificates/

ADD CACerts.pem $HOME/.local/share/unity3d/Certificates/

# got that from here: https://gitlab.com/gableroux/unity3d-gitlab-ci-example/issues/17

# unity 2018 needs jdk 8 https://docs.unity3d.com/Manual/android-sdksetup.html
RUN apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository ppa:openjdk-r/ppa && \
    apt-get update && \
    apt-get install -y openjdk-8-jdk
    
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/jre/
ENV PATH ${PATH}:/usr/lib/jvm/java-8-openjdk-amd64/jre/bin


RUN apt-get update && \
    apt-get install unzip

# ------------------------------------------------------ 
# --- Download Android SDK tools into $ANDROID_HOME
ENV ANDROID_HOME /opt/android-sdk-linux

# newer SDK versions https://stackoverflow.com/questions/37505709/how-do-i-download-the-android-sdk-without-downloading-android-studio
RUN cd /opt && \
    wget -q https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip -O android-sdk.zip && \
    unzip -q android-sdk.zip -d android-sdk-linux && \
    rm -f android-sdk.zip && \
    ls -ahl android-sdk-linux

ENV PATH ${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools

RUN chmod -R 755 .${ANDROID_HOME}/tools/*

# ------------------------------------------------------ 
# --- Install Android SDKs and other build packages 
# https://developer.android.com/studio/command-line/sdkmanager

# accept license
RUN yes | ${ANDROID_HOME}/tools/bin/sdkmanager --licenses

# platform-tools,extra-android-support
RUN ${ANDROID_HOME}/tools/bin/sdkmanager "platform-tools"

# SDKs 
# android-27
RUN ${ANDROID_HOME}/tools/bin/sdkmanager "platforms;android-${ANDROID_PLATFORM}"
 
# ------------------------------------------------------ 
# --- Install Android NDK in $ANDROID_NDK_HOME
# --- ndk-13b needed for il2cpp android-build -> download this version
# --- Error while building Fix that-> Configure project : NDK is missing a "platforms" directory. If you are using NDK, verify the ndk.dir is set to a valid NDK directory. It is currently set to /opt/android-ndk-linux. If you are not using NDK, unset the NDK variable from ANDROID_NDK_HOME or local.properties to remove this warning.
# --- |-> Fixed

ENV ANDROID_NDK_HOME /opt/android-ndk-${NDK_VERSION}

RUN cd /opt && \
	wget -q https://dl.google.com/android/repository/android-ndk-${NDK_VERSION}-linux-x86_64.zip -O android-ndk.zip && \
	unzip -q android-ndk.zip && \
	rm -f android-ndk.zip
	
# ------------------------------------------------------ 
# --- Install Gradle from PPA 
# Gradle PPA 
RUN add-apt-repository ppa:cwchien/gradle
RUN apt-get update && \
    apt-get -y install gradle
RUN gradle -v

# Clean up
RUN rm -rf /tmp/* /var/tmp/*

RUN df -h
